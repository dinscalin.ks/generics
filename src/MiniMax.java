import java.util.Collections;
import java.util.List;

public class MiniMax<I extends Number> {
    private I min;
    private I max;
    private I[] args;
    public MiniMax(I min, I max) {
        this.min = min;
        this.max = max;
    }

    public MiniMax<I> result(I[] args) {

        List list = List.of(args);
        max = (I) Collections.max(list);
        min = (I) Collections.min(list);
        MiniMax<I> miniMax = new MiniMax<>(min, max);
        return miniMax;
    }

    public I getMin() {
        return min;
    }

    public void setMin(I min) {
        this.min = min;
    }

    public I getMax() {
        return max;
    }

    public void setMax(I max) {
        this.max = max;
    }

    @Override
    public String toString() {
        return "MiniMax{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }
}
